import styled from 'styled-components';

interface IStatsProps {
    playerName: string;
    score: number;
    secondsLeft: number;
}

export const Stats: React.FC<IStatsProps> = ({playerName, score, secondsLeft}) => (
    <StyledScoreContainer data-testid="stats-panel">
        <p>player: {playerName}</p>
        <p>score: {score}</p>
        <p>Time left: {secondsLeft}s</p>
    </StyledScoreContainer>
)

const StyledScoreContainer = styled.div`
  position: absolute;
  top: 20px;
  left: 50px;
  background: rgba(255, 255, 255, 0.2);
  border-radius: 16px;
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(5px);
  -webkit-backdrop-filter: blur(5px);
  border: 1px solid rgba(255, 255, 255, 0.3);
  padding: 0px 15px;
`
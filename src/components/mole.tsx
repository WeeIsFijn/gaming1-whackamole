import styled from 'styled-components';
import { IGridPosition } from '../lib/grid-position';

interface IMoleProps {
    // grid position of the mole (0 indexed)
    position: IGridPosition
    // mole visible or not
    isVisible: Boolean,
    // mole hit callback
    onHit?: (position: IGridPosition) => void,
}

const URL_VISIBLE = './WAM_Mole.png'
const URL_HIDDEN = './WAM_Hole.png'

export const Mole: React.FC<IMoleProps> = ({ position, isVisible, onHit }) => {
    const onClick = () => {
        if (!isVisible) return // hidden moles cannot be hit.
        onHit?.(position)
    }

    return (
        <StyledMole backgroundUrl={isVisible ? URL_VISIBLE : URL_HIDDEN} onClick={onClick} data-testid="mole"/>
    )
}

const StyledMole = styled.div<{ backgroundUrl: string }>`
  background: URL('${p => p.backgroundUrl}') no-repeat center center;
  background-size: contain; 
  width: 100px;
  height: 100px;
`
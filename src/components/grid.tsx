import styled from 'styled-components';
import { Configuration } from '../config';
import { IGridPosition } from '../lib/grid-position';
import { Mole } from './mole';

interface IGridProps {
    visibleMole?: IGridPosition | null
    // mole hit callback
    onHit?: (position: IGridPosition) => void,
}

export const Grid: React.FC<IGridProps> = ({ visibleMole = null, onHit }) => {
    const isVisible = (row: number, col: number) => (
        col === visibleMole?.col && row === visibleMole.row
    )

    return (
        <StyledBackground>
            <StyledGridColumn>
            { Configuration.rowIndices.map( rowIndex => (
                <StyledGridRow key={`row-${rowIndex}`} data-testid="grid-row">
                { Configuration.colIndices.map ( colIndex => (
                    <Mole 
                        key={`mole-${rowIndex}${colIndex}`}
                        position={{ row: rowIndex, col: colIndex }}
                        isVisible={isVisible(rowIndex, colIndex)}
                        onHit={onHit} />
                ))}
                </StyledGridRow>
            ))}
            </StyledGridColumn>
        </StyledBackground>
    )
}

const StyledGridColumn = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  height: 100%;
  padding: 40px 200px;
`

const StyledGridRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`

const StyledBackground = styled.div`
  cursor: url('./Hammer_Small.png') 10 10, pointer;
  background: URL('./WAM_bg.jpg') no-repeat center center;
  background-size: cover; 
  width: 100%;
  height: 100%;
`
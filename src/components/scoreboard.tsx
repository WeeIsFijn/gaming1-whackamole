import styled from 'styled-components';
import { ERequestState } from '../lib/request-state';
import { IScoreEntry } from '../lib/score-entry';

interface IScoreboardProps {
    scores?: Array<IScoreEntry> | null;
    getScoresRequestState: ERequestState;
}

export const Scoreboard: React.FC<IScoreboardProps> = ({ scores = [], getScoresRequestState = ERequestState.IDLE }) => (
    <StyledScoreContainer>
        { (getScoresRequestState === ERequestState.FETCHING || getScoresRequestState === ERequestState.IDLE) && (
            <p>Loading...</p>
        )} 
        { (getScoresRequestState === ERequestState.FAILED) && (
            <p>Error loading scoreboard</p>
        )} 
        { (getScoresRequestState === ERequestState.SUCCEEDED) && (
            <div>
                <h1>Scoreboard</h1>
                {scores?.map(score => (
                    <p key={score.playerName}>{score.playerName}: {score.score}</p>
                ))}
            </div>
        )} 
        
    </StyledScoreContainer>
)

const StyledScoreContainer = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  margin-left: auto; 
  margin-right: auto;
  margin-top: 100px;
  width: 20%;
  background: rgba(255, 255, 255, 0.2);
  border-radius: 16px;
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(5px);
  -webkit-backdrop-filter: blur(5px);
  border: 1px solid rgba(255, 255, 255, 0.3);
  padding: 0px 15px;
`
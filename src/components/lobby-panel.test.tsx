import { fireEvent, render, screen } from "@testing-library/react"
import { Configuration } from "../config"
import { LobbyPanel } from "./lobby-panel"

describe('lobby panel', () => {
    it('loads and displays input field', () => {
        render(<LobbyPanel onStart={() => {}} />)
        const input = screen.getByPlaceholderText(/Player Name/)
        expect(input).toBeInTheDocument()
    })

    it('will invoke onStart when starting a game with correct player name and game duration', () => {
        const spy = jest.fn()
        const onStart = (playerName: string, durationSeconds: number) => {
            spy(playerName, durationSeconds)
        }
        const component = render(<LobbyPanel onStart={onStart}/>)
        const input = screen.getByPlaceholderText(/Player Name/)
        const button = screen.getByText(/Start game!/)
        fireEvent.change(input, {target: { value: 'Bob'}})
        fireEvent.click(button)
        expect(spy).toHaveBeenCalledWith('Bob', Configuration.gameDurationSeconds)
    })

    it('will not invoke and show a validation message when no player name was entered', () => {
        const spy = jest.fn()
        const onStart = (playerName: string, durationSeconds: number) => {
            spy(playerName, durationSeconds)
        }
        const component = render(<LobbyPanel onStart={onStart}/>)
        const input = screen.getByPlaceholderText(/Player Name/)
        const button = screen.getByText(/Start game!/)
        fireEvent.click(button)
        const validationMessage = screen.getByText(/Please enter a player name first/)
        expect(spy).not.toHaveBeenCalled()
        expect(validationMessage).toBeInTheDocument()
    })
})
import { fireEvent, render, screen } from "@testing-library/react"
import { debug } from "console"
import { Configuration } from "../config"
import { Grid } from "./grid"
import { Mole } from "./mole"

describe('Mole', () => {
    it('will render mole', () => {
        render(<Mole isVisible={false} position={{ row: 0, col: 0}} />)
        const mole = screen.getByTestId('mole')
        expect(mole).toBeInTheDocument()
    })

    it('will fire onHit when mole is visible and clicked', () => {
        const spy = jest.fn()
        const onHit = () => spy()

        render(<Mole isVisible={true} position={{ row: 0, col: 0}} onHit={onHit} />)
        const mole = screen.getByTestId('mole')
        fireEvent.click(mole)
        expect(spy).toHaveBeenCalled()
    })

    it('will not fire onHit when mole is not visible but clicked', () => {
        const spy = jest.fn()
        const onHit = () => spy()

        render(<Mole isVisible={false} position={{ row: 0, col: 0}} onHit={onHit} />)
        const mole = screen.getByTestId('mole')
        fireEvent.click(mole)
        expect(spy).not.toHaveBeenCalled()
    })
})
import { useState } from 'react';
import styled from 'styled-components';
import { Configuration } from '../config';

interface ILobbyPanelProps {
    // Action that indicates player wants to start the game with playerName
    onStart: (playerName: string, durationSeconds: number) => void
}

export const LobbyPanel: React.FC<ILobbyPanelProps> = ({ onStart }) => {
    const [playerName, setPlayerName] = useState<string>('')
    const [isError, setError] = useState<boolean>(false)

    const onInput: React.ChangeEventHandler<HTMLInputElement> = event => {
        setPlayerName(event.target.value)
    }

    const validate = (): boolean => {
        if (playerName === '') {
            setError(true)
            return false
        }
        return true
    }

    const onStartClick = () => {
        if (!validate()) return
        onStart(playerName, Configuration.gameDurationSeconds)
    }

    const onStartShortClick = () => {
        if (!validate()) return
        onStart(playerName, 10)
    }

    return (
        <StyledMenuContainer>
            <p>Please choose a player name:</p>
            { isError && <StyledError>Please enter a player name first</StyledError> }
            <p><input type="text" placeholder='Player Name' onChange={onInput}/></p>
            <button onClick={onStartClick}>Start game!</button>
            <button onClick={onStartShortClick}>Start debug game (10s duration)</button>
        </StyledMenuContainer>
    )
}

const StyledError = styled.p`
    color: #cc0000;
`
const StyledMenuContainer = styled.div`
  position: absolute;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 15px;

  left: 0;
  right: 0;
  margin-left: auto; 
  margin-right: auto;
  margin-top: 100px;
  width: 20%;

  background: rgba(255, 255, 255, 0.2);
  border-radius: 16px;
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(5px);
  -webkit-backdrop-filter: blur(5px);
  border: 1px solid rgba(255, 255, 255, 0.3);
`
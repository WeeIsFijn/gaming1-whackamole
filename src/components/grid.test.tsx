import { render, screen } from "@testing-library/react"
import { Configuration } from "../config"
import { Grid } from "./grid"

describe('Grid', () => {
    it('renders correct number of rows', () => {
        render(<Grid />)
        const input = screen.getAllByTestId('grid-row')
        expect(input.length).toBe(Configuration.numRows)
    })

    it('renders correct number of moles', () => {
        render(<Grid />)
        const input = screen.getAllByTestId('mole')
        expect(input.length).toBe(Configuration.numRows * Configuration.numCols)
    })
})
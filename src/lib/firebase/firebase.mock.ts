import { IFirebase, IFirebaseEntry } from "./firebase";

export class MockFirebase implements IFirebase {
    public queryReturns: Array<IFirebaseEntry<any>> = []
    public insertSpy: jest.Mock<any, any> | null = null

    public query<T>(collectionName: string): Promise<IFirebaseEntry<T>[]> {
        return new Promise((resolve, _reject) => {
            resolve(this.queryReturns)
        })
    }

    public async insert<T extends { [x: string]: any; }>(collectionName: string, data: T): Promise<void> {
        this.insertSpy?.(collectionName, data)
    }
}
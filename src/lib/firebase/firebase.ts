
import { initializeApp } from "firebase/app";
import { addDoc, collection, Firestore, getDocs, getFirestore, limit, orderBy, OrderByDirection, query } from "firebase/firestore";
import { Secrets } from "../secrets";

export interface IFirebaseEntry<T> {
    id: string
    data: T,
}

export interface IFirebase {
    query<T>(collectionName: string, orderByProp: string, direction: OrderByDirection, limitTo: number): Promise<Array<IFirebaseEntry<T>>>
    insert<T extends { [x: string]: any; }>(collectionName: string, data: T): Promise<void>
}

export class Firebase implements IFirebase {
    private database: Firestore

    public constructor() {
        const config = {
            apiKey: Secrets.firebaseApiKey,
            authDomain: Secrets.firebaseAuthDomain,
            projectId: Secrets.firebaseProjectId,
            storageBucket: Secrets.firebaseStorageBucket,
            messagingSenderId: Secrets.firebaseMessageSenderId,
            appId: Secrets.firebaseAppId,
        }
        const app = initializeApp(config)
        this.database = getFirestore(app)
    }

    public async query<T>(collectionName: string, orderByProp: string, direction: OrderByDirection = 'desc', limitTo: number): Promise<Array<IFirebaseEntry<T>>> {
        const q = query(collection(this.database, collectionName), orderBy(orderByProp, direction), limit(limitTo))
        const snapshot = await getDocs(q)
        var result: Array<IFirebaseEntry<T>> = []
        snapshot.forEach(doc => {
            result.push({
                id: doc.id,
                data: (doc.data() as T),
            })
        })
        return result
    }

    public async insert<T extends { [x: string]: any; }>(collectionName: string, data: T): Promise<void> {
        await addDoc(collection(this.database, collectionName), data)
    }
}
export interface IScoreEntry {
    playerName: string;
    score: number;
}
import { CoreGame, ICoreGame } from "./core-game/core-game"
import { IScoreRepo, ScoreRepo } from "./score-repo/score-repo"

export interface IContainer {
    readonly coreGame: ICoreGame
    readonly scoreRepo: IScoreRepo
}

// Simple container for Dependency Injection (injecting mocks in integration test)
export class Container implements IContainer {
    private static instance: IContainer;

    readonly coreGame: ICoreGame
    readonly scoreRepo: IScoreRepo

    private constructor(coreGame: ICoreGame, scoreRepo: IScoreRepo) {
        this.coreGame = coreGame
        this.scoreRepo = scoreRepo
    }

    public static getInstance(coreGame: ICoreGame = new CoreGame(), scoreRepo: IScoreRepo = new ScoreRepo()): Container {
        if (!Container.instance) {
            Container.instance = new Container(coreGame, scoreRepo);
        }

        return Container.instance;
    }
}

// For demo purposes, secrets are stored in a gitignored .env-file. In production,
// this might not be enough from a security standpoint as the secrets are still packaged
// in the distribution. A backend solution might have to be considered
export class Secrets {
    public static firebaseApiKey: string | undefined = process.env.REACT_APP_FIREB_API_KEY
    public static firebaseAuthDomain: string | undefined = process.env.REACT_APP_FIREB_AUTH_DOMAIN
    public static firebaseProjectId: string | undefined = process.env.REACT_APP_FIREB_PROJECT_ID
    public static firebaseStorageBucket: string | undefined = process.env.REACT_APP_FIREB_STORAGE_BUCKET
    public static firebaseMessageSenderId: string | undefined = process.env.REACT_APP_FIREB_MESSAGE_SENDER_ID
    public static firebaseAppId: string | undefined = process.env.REACT_APP_FIREB_APP_ID
}

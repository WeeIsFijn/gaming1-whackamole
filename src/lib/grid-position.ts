export interface IGridPosition {
    row: number;
    col: number;
}

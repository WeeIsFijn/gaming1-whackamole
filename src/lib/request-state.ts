export enum ERequestState {
    IDLE,
    FETCHING,
    FAILED,
    SUCCEEDED,
}

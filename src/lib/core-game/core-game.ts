import { Configuration } from "../../config";
import { IGridPosition } from "../grid-position";
import { IRandomNumberGenerator, RandomNumberGenerator } from "../rng/rng";

export interface ICoreGame {
    start: () => void;
    stop: () => void;
    scheduleStop: (inSeconds: number) => void;
    onRevealMole: (callback: (position: IGridPosition) => void) => void;
    onGameEnd: (callback: () => void) => void;
    onTimeLeftUpdate: (callback: (seconds: number) => void) => void;
}

export class CoreGame implements ICoreGame {
    private rng: IRandomNumberGenerator
    private moleRevealCallback: ((position: IGridPosition) => void) | null = null
    private endCallback: (() => void) | null = null
    private timeLeftCallback: ((seconds: number) => void) | null = null
    // TODO: Duplicate source of truth with EGameState
    private started: Boolean = false
    private stopTimeout: NodeJS.Timeout | null = null
    private moleRevealTimeout: NodeJS.Timeout | null = null
    private gameTimerTimeout: NodeJS.Timeout | null = null
    private secondsLeft: number | null = null

    constructor(
        rng: IRandomNumberGenerator = new RandomNumberGenerator(),
    ) {
        this.rng = rng
    }

    public start() {
        if (this.started === true) return
        this.started = true
        this.scheduleNextMoleReveal()
    }

    public stop() {
        this.stopTimeout && clearTimeout(this.stopTimeout)
        this.moleRevealTimeout && clearTimeout(this.moleRevealTimeout)
        this.gameTimerTimeout && clearTimeout(this.gameTimerTimeout)
        this.stopTimeout = null
        this.moleRevealTimeout = null
        this.gameTimerTimeout = null
        this.endCallback?.()
        this.started = false
    }

    public scheduleStop(inSeconds: number) {
        this.secondsLeft = inSeconds
        this.stopTimeout = setTimeout(() => {
            this.stop()
        }, inSeconds * 1000)
        this.gameTimerTimeout = setTimeout(() => {
            this.updateGameTimer()
        }, 1000)
    }

    public onRevealMole(callback: (position: IGridPosition) => void) {
        this.moleRevealCallback = callback
    }

    public onGameEnd(callback: () => void) {
        this.endCallback = callback
    }

    public onTimeLeftUpdate(callback: (seconds: number) => void) {
        this.timeLeftCallback = callback
    }

    private scheduleNextMoleReveal() {
        const revealAfter = this.rng.float(
            Configuration.moleRevealTimeMinSeconds * 1000, 
            Configuration.moleRevealTimeMaxSeconds * 1000
        )

        this.moleRevealTimeout = setTimeout(() => {
            if (this.moleRevealCallback != null) {
                const row = this.rng.integer(0, Configuration.numRows - 1)
                const col = this.rng.integer(0, Configuration.numCols - 1)
                this.moleRevealCallback({ row: row, col: col })
                if (this.started === true) {
                    this.scheduleNextMoleReveal()
                }
            }
        }, revealAfter)
    }

    private updateGameTimer() {
        if (!this.started || !this.secondsLeft) return
        this.secondsLeft -= 1
        this.timeLeftCallback?.(this.secondsLeft)
        this.gameTimerTimeout = setTimeout(() => {
            this.updateGameTimer()
        }, 1000)
    }
}
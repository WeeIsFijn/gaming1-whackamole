import { Configuration } from "../../config"
import { CoreGame } from "./core-game"
import { IGridPosition } from "../grid-position"
import { MockRandomNumberGenerator } from "../rng/rng.mock"

// TODO: Configuration can be mocked to reduce mole-reveal-time to speed up this test.
describe('Core game', () => {
    const mockRng = new MockRandomNumberGenerator(0)

    it('should start revealing moles when the game is started', done => {
        const coreGame = new CoreGame(mockRng)
        const callback = (position: IGridPosition) => {
            expect(position.row).toBe(0)
            expect(position.col).toBe(0)
            done()
        }
        coreGame.onRevealMole(callback)
        coreGame.start()
    }, 2000)

    it('should not be revealing moles when the game is stopped', done => {
        const coreGame = new CoreGame(mockRng)
        const callBackInner = jest.fn()
        const callback = (position: IGridPosition) => {
            callBackInner()
        }

        coreGame.onRevealMole(callback)
        coreGame.stop()
        setTimeout(() => {
            done()
        }, 900)
        expect(callBackInner).not.toBeCalled()
    }, 1000)

    it('should never reveal moles out of bounds', done => {
        const mockRng = new MockRandomNumberGenerator(1)
        const coreGame = new CoreGame(mockRng)
        const callback = (position: IGridPosition) => {
            expect(position.row).toBe(Configuration.numRows - 1)
            expect(position.col).toBe(Configuration.numCols - 1)
            done()
        }
        coreGame.onRevealMole(callback)
        coreGame.start()
    }, 3000)
})
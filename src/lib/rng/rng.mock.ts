import { IRandomNumberGenerator } from "./rng";

export class MockRandomNumberGenerator implements IRandomNumberGenerator {
    constructor(
        private mockNormalized: number,
    ) {}

    public float(min: number, max: number) {
        const normalized = this.mockNormalized
        return min + normalized * (max - min)
    }

    public integer(min: number, max: number) {
        const normalized = this.mockNormalized
        return Math.round(min + normalized * (max - min))
    }
}
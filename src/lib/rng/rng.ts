export interface IRandomNumberGenerator {
    float: (min: number, max: number) => number
    integer: (min: number, max: number) => number
}

export class RandomNumberGenerator implements IRandomNumberGenerator {
    public float(min: number, max: number) {
        const normalized = Math.random()
        return min + normalized * (max - min)
    }

    public integer(min: number, max: number) {
        const normalized = Math.random()
        return Math.round(min + normalized * (max - min))
    }
}
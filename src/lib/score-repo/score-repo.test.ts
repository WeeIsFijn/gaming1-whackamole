import { MockFirebase } from "../firebase/firebase.mock"
import { ScoreRepo } from "./score-repo"

describe('Score Repository', () => {
    const mockFirebase = new MockFirebase()

    describe('getting scores', () => {
        it('should get scores', done => {
            mockFirebase.queryReturns = [
                { id: '1', data: { playerName: 'a', score: 300 }},
                { id: '2', data: { playerName: 'b', score: 200 }},
                { id: '3', data: { playerName: 'c', score: 100 }},
            ]
            const sut = new ScoreRepo(mockFirebase)
            sut.getTopTenScores().then(result => {
                expect(result.length).toBe(3)
                expect(result[0].score).toBe(300)
                expect(result[0].playerName).toBe('a')
                expect(result[1].score).toBe(200)
                expect(result[1].playerName).toBe('b')
                expect(result[2].score).toBe(100)
                expect(result[2].playerName).toBe('c')
                done()
            })
        }, 100)

        it('should sort scores from high to low', done => {
            mockFirebase.queryReturns = [
                { id: '1', data: { playerName: 'a', score: 300 }},
                { id: '2', data: { playerName: 'b', score: 100 }},
                { id: '3', data: { playerName: 'c', score: 200 }},
            ]
            const sut = new ScoreRepo(mockFirebase)
            sut.getTopTenScores().then(result => {
                expect(result.length).toBe(3)
                expect(result[0].score).toBe(300)
                expect(result[0].playerName).toBe('a')
                expect(result[1].score).toBe(200)
                expect(result[1].playerName).toBe('c')
                expect(result[2].score).toBe(100)
                expect(result[2].playerName).toBe('b')
                done()
            })
        }, 100)

        it('should ignore bogus or incomplete entries', done => {
            mockFirebase.queryReturns = [
                { id: '1', data: { playerName: 'noscore' }},
                { id: '2', data: { bogus: true }},
                { id: '3', data: { playerName: 'c', score: 200 }},
            ]
            const sut = new ScoreRepo(mockFirebase)
            sut.getTopTenScores().then(result => {
                expect(result.length).toBe(1)
                expect(result[0].score).toBe(200)
                expect(result[0].playerName).toBe('c')
                done()
            })
        }, 100)

        it('should be able to fetch scores with 0 points', done => {
            mockFirebase.queryReturns = [
                { id: '1', data: { playerName: 'a', score: 0 }},
            ]
            const sut = new ScoreRepo(mockFirebase)
            sut.getTopTenScores().then(result => {
                expect(result.length).toBe(1)
                expect(result[0].score).toBe(0)
                expect(result[0].playerName).toBe('a')
                done()
            })
        }, 100)
    })

    describe('saving scores', () => {
        it('should save a score in firebase', done => {
            const spy = jest.fn()
            mockFirebase.insertSpy = spy
            const sut = new ScoreRepo(mockFirebase)
            sut.addScore({ playerName: 'Wouter', score: 100 }).then(r => {
                expect(spy).toHaveBeenCalledWith('scoreboard', { playerName: 'Wouter', score: 100 })
                done()
            })
        })
    })
})
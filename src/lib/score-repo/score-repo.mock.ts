import { IScoreEntry } from "../score-entry";
import { IScoreRepo } from "./score-repo";

export class MockScoreRepo implements IScoreRepo {
    getTopTenScores(): Promise<IScoreEntry[]> {
        return new Promise((resolve, _) => resolve([{ playerName: 'Iris', score: 900 }]))
    }
    addScore(score: IScoreEntry): Promise<void> {
        return new Promise((resolve, _) => resolve())
    }
}
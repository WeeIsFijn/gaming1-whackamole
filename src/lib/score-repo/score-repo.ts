import { Firebase, IFirebase } from "../firebase/firebase";
import { IScoreEntry } from "../score-entry";

export interface IScoreRepo {
    getTopTenScores(): Promise<Array<IScoreEntry>>;
    addScore(score: IScoreEntry): Promise<void>;
}
export class ScoreRepo implements IScoreRepo {
    private COLLECTION = 'scoreboard'
    constructor(
        private firebase: IFirebase = new Firebase()
    ){}

    public async getTopTenScores(): Promise<Array<IScoreEntry>>{
        const results = await this.firebase.query<IScoreEntry>(this.COLLECTION, 'score', 'desc', 10)
        return results
            .filter(r => r.data.score !== undefined && r.data.playerName)
            .map(r => ({
                score: r.data.score,
                playerName: r.data.playerName,
            }))
            .sort((a, b) => b.score - a.score)
    }

    public async addScore(score: IScoreEntry): Promise<void> {
        await this.firebase.insert(this.COLLECTION, score)
    }
}
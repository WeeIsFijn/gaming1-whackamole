import { configureStore } from "@reduxjs/toolkit";
import gameReducer from './game'
import scoreboardReducer from './scoreboard'

const store = configureStore({
    reducer: {
        game: gameReducer,
        scoreboard: scoreboardReducer,
    }
})

export default store

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

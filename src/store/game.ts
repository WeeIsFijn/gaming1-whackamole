import { AnyAction, createSlice, PayloadAction, ThunkAction } from "@reduxjs/toolkit";
import { RootState } from ".";
import { Configuration } from "../config";
import { Container } from "../lib/container";
import { CoreGame } from "../lib/core-game/core-game";
import { EGameState } from "../lib/game-state";
import { IGridPosition } from "../lib/grid-position";
import { IScoreEntry } from "../lib/score-entry";
import { getScores, saveScore } from "./scoreboard";

interface IGameSlice {
    visibleMole: IGridPosition | null;
    scoreEntry: IScoreEntry;
    gameState: EGameState;
    secondsLeft: number;
}

const initialState: IGameSlice = {
    visibleMole: null,
    scoreEntry: {
        playerName: 'PlayerUnknown',
        score: 0,
    },
    gameState: EGameState.LOBBY,
    secondsLeft: 0,
}

const game = Container.getInstance().coreGame

export const startGame = (durationSeconds: number): ThunkAction<void, RootState, unknown, AnyAction> => 
async (dispatch, getState) => {
    
    game.onRevealMole((position) => {
        dispatch(gameSlice.actions.setVisibleMole(position))
    })
    game.onGameEnd(async () => {
        const state = getState()
        dispatch(gameSlice.actions.setGameState(EGameState.ENDED))
        await dispatch(saveScore(state.game.scoreEntry))
        dispatch(getScores)
    })
    game.onTimeLeftUpdate((seconds) => {
        dispatch(gameSlice.actions.setSecondsLeft(seconds))
    })
    game.start()
    game.scheduleStop(durationSeconds)
    dispatch(gameSlice.actions.setSecondsLeft(durationSeconds))
    dispatch(gameSlice.actions.setGameState(EGameState.PLAYING))
}

export const onHitMole: ThunkAction<void, RootState, unknown, AnyAction> = 
(dispatch, _getState) => {
    dispatch(gameSlice.actions.setVisibleMole(null))
    dispatch(gameSlice.actions.incrementScore(Configuration.pointsPerHit))
}

export const gameSlice = createSlice({
    name: 'game',
    initialState: initialState,
    reducers: {
        setVisibleMole: (state, payload: PayloadAction<IGridPosition|null>) => {
            state.visibleMole = payload.payload
        },
        incrementScore: (state, payload: PayloadAction<number>) => {
            state.scoreEntry.score += payload.payload
        },
        setPlayerName: (state, payload: PayloadAction<string>) => {
            state.scoreEntry.playerName = payload.payload
        },
        setGameState: (state, payload: PayloadAction<EGameState>) => {
            state.gameState = payload.payload
        },
        setSecondsLeft: (state, payload: PayloadAction<number>) => {
            state.secondsLeft = payload.payload
        },
    }
})

export const { setPlayerName } = gameSlice.actions
export default gameSlice.reducer
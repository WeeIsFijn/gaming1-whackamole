import { AnyAction, createSlice, PayloadAction, ThunkAction } from "@reduxjs/toolkit";
import { RootState } from ".";
import { Container } from "../lib/container";
import { ERequestState } from "../lib/request-state";
import { IScoreEntry } from "../lib/score-entry";
import { ScoreRepo } from "../lib/score-repo/score-repo";

interface IScoreboardSlice {
    getScoresRequestState: ERequestState,
    saveScoreRequestState: ERequestState,
    scores: Array<IScoreEntry>;
}

const initialState: IScoreboardSlice = {
    getScoresRequestState: ERequestState.IDLE,
    saveScoreRequestState: ERequestState.IDLE,
    scores: [],
}

const scoreRepo = Container.getInstance().scoreRepo

export const getScores: ThunkAction<void, RootState, unknown, AnyAction> = 
(dispatch, _getState) => {
    dispatch(scoreboardSlice.actions.setGetScoresRequestState(ERequestState.FETCHING))
    scoreRepo
        .getTopTenScores()
        .then(result => {
            dispatch(scoreboardSlice.actions.setScores(result))
            dispatch(scoreboardSlice.actions.setGetScoresRequestState(ERequestState.SUCCEEDED))
        })
        .catch(() => {
            dispatch(scoreboardSlice.actions.setGetScoresRequestState(ERequestState.FAILED))
        })
}

export const saveScore = (score: IScoreEntry): ThunkAction<void, RootState, unknown, AnyAction> => 
async (dispatch, _getState) => {
    try {
        dispatch(scoreboardSlice.actions.setSaveScoreRequestState(ERequestState.FETCHING))
        await scoreRepo.addScore(score)
        dispatch(scoreboardSlice.actions.setSaveScoreRequestState(ERequestState.SUCCEEDED))
    } catch {
        dispatch(scoreboardSlice.actions.setSaveScoreRequestState(ERequestState.FAILED))
    }
}

export const scoreboardSlice = createSlice({
    name: 'scoreboard',
    initialState: initialState,
    reducers: {
        setGetScoresRequestState: (state, payload: PayloadAction<ERequestState>) => {
            state.getScoresRequestState = payload.payload
        },
        setSaveScoreRequestState: (state, payload: PayloadAction<ERequestState>) => {
            state.saveScoreRequestState = payload.payload
        },
        setScores: (state, payload: PayloadAction<Array<IScoreEntry>>) => {
            state.scores = payload.payload
        },
    }
})

export default scoreboardSlice.reducer
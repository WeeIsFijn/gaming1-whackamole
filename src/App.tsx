import './App.css';
import { Grid } from './components/grid';
import { IGridPosition } from './lib/grid-position';
import { onHitMole, setPlayerName, startGame } from './store/game';
import { useTypedDispatch, useTypedSelector } from './store/hooks';
import styled from 'styled-components';
import { Stats } from './components/stats';
import { LobbyPanel } from './components/lobby-panel';
import { EGameState } from './lib/game-state';
import { Scoreboard } from './components/scoreboard';

function App() {
  const visibleMole = useTypedSelector(state => state.game.visibleMole)
  const score = useTypedSelector(state => state.game.scoreEntry.score)
  const scores = useTypedSelector(state => state.scoreboard.scores)
  const getScoresRequestState = useTypedSelector(state => state.scoreboard.getScoresRequestState)
  const playerName = useTypedSelector(state => state.game.scoreEntry.playerName)
  const gameState = useTypedSelector(state => state.game.gameState)
  const secondsLeft = useTypedSelector(state => state.game.secondsLeft)
  const dispatch = useTypedDispatch()

  const onHit = (position: IGridPosition) => {
    dispatch(onHitMole)
  }

  const onStart = (playerName: string, durationSeconds: number) => {
    dispatch(setPlayerName(playerName))
    dispatch(startGame(durationSeconds))
  }

  return (
    <StyledContainer>
      {gameState === EGameState.ENDED && <Scoreboard scores={scores} getScoresRequestState={getScoresRequestState} />}
      {gameState === EGameState.LOBBY && (<LobbyPanel onStart={onStart}/>)}
      {gameState === EGameState.PLAYING && (<Stats playerName={playerName} score={score} secondsLeft={secondsLeft} />)}
      <Grid visibleMole={visibleMole} onHit={onHit} />
    </StyledContainer>
  );
}



const StyledContainer = styled.div`
  position: relative;
  background-color: orange;
  height: 100vh;
`

export default App;

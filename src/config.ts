export class Configuration {
    // Number of rows in the mole grid
    public static numRows = 3
    // Number of columns in the mole grid
    public static numCols = 4
    public static moleRevealTimeMinSeconds = 1.0
    public static moleRevealTimeMaxSeconds = 2.0
    public static pointsPerHit = 100
    public static gameDurationSeconds = 120

    // Utility ordered array with the index of each row (for iteration)
    public static rowIndices = [ ...Array(Configuration.numRows).keys() ]
    // Utility ordered array with the index of each row (for iteration)
    public static colIndices = [ ...Array(Configuration.numCols).keys() ]
}
import { fireEvent, render, screen } from "@testing-library/react"
import { Provider } from "react-redux"
import App from "../App"
import { Container } from "../lib/container"
import { CoreGame } from "../lib/core-game/core-game"
import { MockScoreRepo } from "../lib/score-repo/score-repo.mock"
import store from "../store"

describe('Game states', () => {
    it('should show the lobby panel with player input before game start', () => {
        const container = Container.getInstance(new CoreGame(), new MockScoreRepo())
        render(
            <Provider store={store}>
                <App />
            </Provider>
        )
        const input = screen.getByPlaceholderText(/Player Name/)
        expect(input).toBeInTheDocument()
    })

    it('should show the stats panel with player name when game did start', () => {
        const container = Container.getInstance(new CoreGame(), new MockScoreRepo())
        render(
            <Provider store={store}>
                <App />
            </Provider>
        )

        const button = screen.getByText(/Start game!/)
        const input = screen.getByPlaceholderText(/Player Name/)
        fireEvent.change(input, {target: { value: 'Marjan'}})
        fireEvent.click(button)
        const testPanel = screen.getByTestId("stats-panel")
        const playerLabel = screen.getByText(/Marjan/)
        expect(testPanel).toBeInTheDocument()
        expect(playerLabel).toBeInTheDocument()
    })
})